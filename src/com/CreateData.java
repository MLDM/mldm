package com;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;


public class CreateData {

    /** create HashMap of Atrributes */
    public static LinkedHashMap<String, String> createAttr(ArrayList<String[]> hamSpam, ArrayList<String> words) {
        LinkedHashMap<String, String> result = new LinkedHashMap<>();

        for (int i = 0; i < words.size(); i++) {
            result.put(words.get(i), "INTEGER");
        }

        result.put("is_forwarded", "{0,1}");
        result.put("has_link", "{0,1}");
        result.put("wordcount_subject", "INTEGER");
        result.put("wordcount_body", "INTEGER");
        result.put("special_char_count", "INTEGER");
        result.put("diversity", "INTEGER");
        result.put("class", "{0,1}");

        return result;
    }

    /** create List of data */
    public static ArrayList<ArrayList<String>> createData(ArrayList<String[]> hamSpam, ArrayList<String> words) {
        ArrayList<ArrayList<String>> result = new ArrayList<>();
        ArrayList<String> list = new ArrayList<>();


        for (int i = 0; i < hamSpam.size(); i++) {
            list = new ArrayList<>();

            for (int j = 0; j < words.size(); j++) {
                list.add(String.valueOf(Utils.searchForWord(hamSpam.get(i)[2], words.get(j))));
            }

            if (Utils.isForwarded(hamSpam.get(i)[1]) || Utils.isForwarded(hamSpam.get(i)[2])) {
                list.add("1");
            } else {
                list.add("0");
            }


            if (Utils.hasLink(hamSpam.get(i)[2])) {
                list.add("1");
            } else {
                list.add("0");
            }

            list.add(String.valueOf(Utils.wordCount(hamSpam.get(i)[1])));
            list.add(String.valueOf(Utils.wordCount(hamSpam.get(i)[2])));
            list.add(String.valueOf(Utils.specialCharCount(hamSpam.get(i)[2])));
            list.add(String.valueOf(Utils.checkDiversity(hamSpam.get(i)[2])));

            if (Utils.isSpam(hamSpam.get(i)[3])){
                list.add("0");
            } else {
                list.add("1");
            }


            result.add(list);
        }

        return result;
    }
}
