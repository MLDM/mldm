package com;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Main {

	public static void main(String[] args) throws Exception {

	    // read in csv file
		String csvFile = "./enron.csv";
		String line = "";
		String cvsSplitBy = ",";
		ArrayList<String[]> ham = new ArrayList<String[]>();
        ArrayList<String[]> spam = new ArrayList<String[]>();


        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
			br.readLine();
			while ((line = br.readLine()) != null) {

				// use comma as separator
				String[] column = line.split(cvsSplitBy);
				 if (Integer.parseInt(column[3]) == 1) {
					ham.add(column);
				} else {
					spam.add(column);
				}

			}

		} catch (IOException e) {
			e.printStackTrace();
		}


        ArrayList<String> words = new ArrayList<>();

        // top10 words of subject ham
        LinkedHashMap<String, Integer> attr = new LinkedHashMap<String, Integer>(Utils.getTop10(Utils.removeStopKeys(Utils.getWordCountListSubject(ham))));
        for (Map.Entry<String, Integer> entry : attr.entrySet()) {
            words.add(entry.getKey());
        }

        // top10 words of body ham
        attr = new LinkedHashMap<String, Integer>(Utils.getTop10(Utils.removeStopKeys(Utils.getWordCountListBody(ham))));
        for (Map.Entry<String, Integer> entry : attr.entrySet()) {
            words.add(entry.getKey());
        }

        // top10 words of subject spam
        attr = new LinkedHashMap<String, Integer>(Utils.getTop10(Utils.removeStopKeys(Utils.getWordCountListSubject(spam))));
        for (Map.Entry<String, Integer> entry : attr.entrySet()) {
            words.add(entry.getKey());
        }

        // top10 words of body spam
        attr = new LinkedHashMap<String, Integer>(Utils.getTop10(Utils.removeStopKeys(Utils.getWordCountListBody(spam))));
        for (Map.Entry<String, Integer> entry : attr.entrySet()) {
            words.add(entry.getKey());
        }

        Set<String> hs = new LinkedHashSet<>(words);
        words.clear();
        words.addAll(hs);

        LinkedHashMap<String, String> allAttr = CreateData.createAttr(ham, words);
        allAttr.putAll(CreateData.createAttr(spam, words));

        ArrayList<ArrayList<String>> allData = CreateData.createData(ham, words);
        allData.addAll(CreateData.createData(spam, words));

        ArffWriter.write(allAttr, allData);

	}

}
