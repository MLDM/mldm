package com;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {
	static Integer i = 0;

	/** resturns HashMap with words that occur most within subject */
	public static HashMap<String, Integer> getWordCountListSubject(ArrayList<String[]> columns) {
		HashMap<String, Integer> result = new HashMap<>();
		for (Iterator<String[]> iterator = columns.iterator(); iterator.hasNext();) {
			String[] column = (String[]) iterator.next();
			String[] words = column[1].split(" ");
			for (int i = 0; i < words.length; i++) {
				if (result.containsKey(words[i].trim())) {
					result.put(words[i], result.get(words[i]) + 1);
				} else {
					if (!words[i].trim().isEmpty() && words[i] != null && words[i].trim().matches("^[a-zA-Z]+$")) {
						result.put(words[i].trim(), 1);
					}
				}
			}
		}
		return result;
	}

    /** resturns HashMap with words that occur most within body */
	public static HashMap<String, Integer> getWordCountListBody(ArrayList<String[]> columns) {
		HashMap<String, Integer> result = new HashMap<>();
		for (Iterator<String[]> iterator = columns.iterator(); iterator.hasNext();) {
			String[] column = (String[]) iterator.next();
			String[] words = column[2].split(" ");
			for (int i = 0; i < words.length; i++) {
				if (result.containsKey(words[i].trim())) {
					result.put(words[i], result.get(words[i]) + 1);
				} else {
					if (!words[i].trim().isEmpty() && words[i] != null && words[i].trim().matches("^[a-zA-Z]+$")) {
						result.put(words[i].trim(), 1);
					}
				}
			}
		}
		return result;
	}

	public static void printMap(Map<String, Integer> map) {
		for (Entry<String, Integer> entry : map.entrySet()) {
			System.out.println("Word: " + entry.getKey() + "; Count: " + entry.getValue());
		}
	}

	public static Map getTop10(Map<String, Integer> map) {
		i=0;
		Map<String, Integer> result = new HashMap<>(map);
		while (result.size() > 10) {
			result.entrySet().removeIf(entry -> result.get(entry.getKey()).intValue() <= i);
			i++;
		}
		return result;

	}
	

    public static int wordCount(String text) {
        int wordCount = 1;

        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) == ' ') {
                wordCount++;
            }
        }
        return wordCount;
    }

    public static int checkDiversity(String text) {
        int count = 0;

        for (int i = 1; i < text.length(); i++) {
            if (text.charAt(i) == text.charAt(i-1)) {
                count++;
            }
        }
        return count;
    }

    public static int specialCharCount(String text) {
        int wordCount = 0;
        Pattern regex = Pattern.compile("[$&+(){}%§!=?@#|]");
        Matcher matcher;

        for (int i = 0; i < text.length(); i++) {

            String token = new String(String.valueOf(text.charAt(i)));
            matcher = regex.matcher(token);
            if (matcher.find()) {
                wordCount++;
            }

        }
        return wordCount;
    }

    public static int searchForWord(String text, String word) {
        int count = 0;
        Pattern regex = Pattern.compile(word);
        Matcher matcher = regex.matcher(text);;

        while (matcher.find()){
            count +=1;
        }
        return count;
    }

    public static boolean isForwarded(String text) {

        if (text.contains("re :") == true || text.contains("forwarded") == true) {
            return true;
        } else {
            return false;
        }
    }


    public static boolean hasLink(String text) {
        if (text.contains("http:") == true || text.contains("www.") == true || text.contains(".com") == true) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isSpam(String text) {

        if (text.contains("1") == true) {
            return true;
        } else {
            return false;
        }
    }

    public static Map removeStopKeys(Map<String, Integer> map) {
        HashMap<String, Integer> result = new HashMap<String, Integer>(map);
        Set<String> stopwords = new HashSet<>();
        try (BufferedReader br = new BufferedReader(new FileReader("./Stopwordlist"))) {
            String line = "";
            while ((line = br.readLine()) != null) {
                stopwords.add(line);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        for (Iterator iterator = stopwords.iterator(); iterator.hasNext(); ) {
            String string = (String) iterator.next();
            if (result.containsKey(string)) {
                result.remove(string);
            }
        }
        return result;
    }
}
