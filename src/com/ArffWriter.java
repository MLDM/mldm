package com;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.*;

public class ArffWriter {
	/**
	 * takes 2 arguments: - CSV input file - ARFF output file
	 */

	public static void write(LinkedHashMap<String,String> attr, ArrayList<ArrayList<String>> data) throws Exception {


		try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream("./spamfile.arff"), "utf-8"))) {
			writer.write("@relation spamfile");
			writer.newLine();
			writer.newLine();
			for (Map.Entry<String, String> entry : attr.entrySet()) {
				writer.write("@attribute " + entry.getKey() + " " + entry.getValue());
				writer.newLine();
			}
			writer.newLine();
			writer.write("@data");
			writer.newLine();

			for (int i = 0; i < data.size(); i++) {
				writer.write(data.get(i).toString().replace("[","").replace("]",""));
				writer.newLine();

			}

		}

	}
}